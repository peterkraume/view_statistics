# View-Statistics Change-Log



## 2017-11-23  Release of version 1.0.2

### 2017-11-23  Thomas Deuling  <typo3@coding.ms>

*	[BUGFIX] Fixing of tracking IP addresses

### 2017-11-21  Thomas Deuling  <typo3@coding.ms>

*	[BUGFIX] Adding group by in object SQL statement



## 2017-11-20  Release of version 1.0.1

### 2017-11-20  Thomas Deuling  <typo3@coding.ms>

*	[BUGFIX] Fixing sort ViewHelper



## 2017-11-19  Release of version 1.0.0

### 2017-11-16  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Tracking IP-Address optionally
*	[FEATURE] Tracking for Referrer, Request-URI and language
*	[FEATURE] Restrictions for non admins
*	[TASK] Translations
*	[TASK] CSV-Export for Tracks and Frontend-User
*	[TASK] Backend-List for custom objects
